<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;

use JWTAuth;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Serializer\EmberSerializer;
use App\Transformers\UserTransformer;


class UserController extends ApiController
{

    public function __construct()
    {
        $this->middleware('jwt.auth', ['except' => ['authenticate','store']]);
    }

    public function index()
    {
        $user = \Auth::User();

        return fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('user')
            ->toArray();
    }


    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = \Auth::User();


        // if no errors are encountered we can return a JWT
        return response()->json(array('token' => $token));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->get('user'), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(
                array('errors' => $validator->messages()),
                422
            );
        }

        $user = new \App\User($request->get('user'));
        $user->save();

        return fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('user')
            ->toArray();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email|unique:users,email|max:9999999999',
            'password' => 'required|string',
            'password_extra' => 'required|string'
        ]);

        if ($validator->fails()) {
            return response()->json(
                array('errors' => $validator->messages()),
                422
            );
        }

        $user = \Auth::User();

        if($user->id !== $id){
            return response()->json(
                array('errors' => array('user' => 'User does not have permissions to update this user!')),
                422
            );
        }

        $user->fill($request->all());
        $user->save();

        return fractal()
            ->item($user, new UserTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('user')
            ->toArray();

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $user = \Auth::User();


        if($user->id !== $id){
            return response()->json(
                array('errors' => array('user' => 'User does not have permissions to delete this user!')),
                422
            );
        }

        $user->delete();

        return response()->json(array(true));
    }
}
