<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\ApiController;
use JWTAuth;
use Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Serializer\EmberSerializer;
use App\Transformers\PostTransformer;

class PostController extends ApiController
{
    public function __construct()
    {

        // Apply the jwt.auth middleware to all methods in this controller
        // except for the authenticate method. We don't want to prevent
        // the user from retrieving their token if they don't already have it
        $this->middleware('jwt.auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \Auth::User();
        $posts = $user->Posts()->get();

        return fractal()
            ->collection($posts, new PostTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('posts')
            ->toArray();
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255',
            'description' => 'string|max:255'
        ]);

        if ($validator->fails()) {
            return response()->json(
                array('errors' => $validator->messages()),
                422
            );
        }

        $user = \Auth::User();
        $post = new \App\Post($request->get('post'));
        
        $user->Posts()->save($post);

        return fractal()
            ->item($post, new PostTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('post')
            ->toArray();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = \Auth::User();
        $post = $user->Posts()->find($id);

        if(!$post){
            return response()->json(
                array('errors' => array('post' => 'User does not have permissions to access this post!')),
                422
            );
        }

        return fractal()
            ->item($post, new PostTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('post')
            ->toArray();
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'string|max:255',
            'description' => 'string|max:255',
            'status' => 'string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json(
                array('errors' => $validator->messages()),
                422
            );
        }

        $user = \Auth::User();
        $post = $user->Posts()->find($id);

        if(!$post){
            return response()->json(
                array('errors' => array('post' => 'User does not have permissions to update this post!')),
                422
            );
        }
        $post->fill($request->get('post'));
        $post->save();

        return fractal()
            ->item($post, new PostTransformer())
            ->serializeWith(new EmberSerializer())
            ->resourceName('post')
            ->toArray();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = $user = \Auth::User();
        $post = $user->Posts()->find($id);

        if(!$post){
            return response()->json(
                array('errors' => array('post' => 'User does not have permissions to delete this post!')),
                422
            );
        }

        $post->delete();

        return response()->json(array(true));
    }
}
