<?php
namespace App\Transformers;

use App\Company;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract
{


    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\App\Post $post)
    {

        return [
            'id' => $post['id'],
            'title' => $post['title'],
            'description' => $post['description'],
            'user_id' => $post['user_id']
        ];
    }


}