<?php
namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * Turn this item object into a generic array
     *
     * @return array
     */
    public function transform(\App\User $user)
    {
        $posts = $user->Posts->keyBy('id')->keys()->toArray();
        return [
            'id' => $user['id'],
            'name' => $user['name'],
            'email' => $user['email'],
            'post_ids' => $posts
        ];
    }

}